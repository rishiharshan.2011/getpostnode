const express = require('express');
const mongoose = require('mongoose');
const router=require('./routes/route')
const url ='mongodb+srv://test:test123@cluster0.c8nn6.mongodb.net/myFirstDatabase?retryWrites=true&w=majority' ;

const app= express();
mongoose.connect(url,{ useNewUrlParser: true },()=>{
    console.log('database connected');
})

app.use(express.json());
app.use('/',router);

app.listen(5000);