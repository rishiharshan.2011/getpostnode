const express=require('express');
const Post=require('../models/post');

const router = express.Router();
router.post('/',async (req,res)=>{
    const post1= new Post({
        username:req.body.username,
        password:req.body.password
    })
    try{
        const save = await post1.save();
        res.json(save);
    }catch(err){
        res.send('Error');
    }
    
    
    
});
router.get('/',async (req,res)=>{
    try
    {
        const post = await Post.find();
        res.json(post);
    }
    catch{
        res.send('Error');
    }
})

module.exports=router;